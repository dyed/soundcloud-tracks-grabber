FROM node:21-bookworm-slim

WORKDIR /app

COPY package*.json ./

RUN npm i

COPY . .

RUN npx tsc

CMD [ "node", "./dist/main.js" ]