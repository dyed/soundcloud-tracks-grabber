import TelegramBot from 'node-telegram-bot-api'
import { db } from '../db/drizzle-client.js'
import { tracks } from '../db/schemas/tracks.js'
import { Artist } from '../db/schemas/artists.js'
import { clientId, getArtistTracks, getTrackFileUrl } from '../soundcloud-api.js'
import { supabase } from '../supabase.js'
import { TracksReceiverChat, tracksReceiverChats } from '../db/schemas/tracks-receiver-chats.js'

// this function is smelly large pile of shit , definitely need to split it
export async function grabNewTracks(botApi : TelegramBot) {
    console.log('grabbing tracks with :' + clientId)

    const receivers = await db.select().from(tracksReceiverChats)
    const artists = await db.query.artists.findMany({ with: { tracks: true } })

    let errorNotificationSent = false

    for (const artist of artists) {
        try {
            await sendArtistNewTracks(artist, receivers, botApi)
        }
        catch(error) {
            if(!errorNotificationSent) {
                receivers.forEach(receiver => {
                    botApi.sendMessage(receiver.id,
                        'error in scheduled track grabbing, sc client id is prob ' +
                        'expired. error: ' + error)
                })
                errorNotificationSent = true
            }
        }
    }

    console.log('finished')
}

async function sendArtistNewTracks(artist : Artist,
        newTracksReceivers : TracksReceiverChat[], botApi : TelegramBot) {
    const tracksFromApi = await getArtistTracks(artist.id)

    tracksFromApi.collection.forEach(async (trackFromApi) => {
        if(!artist.tracks.find(track => track.id === trackFromApi.id) || artist.tracks.length === 0) {
            const trackFileUrl = await getTrackFileUrl(trackFromApi)
            const fileResponse = await fetch(trackFileUrl)

            if(!fileResponse.ok) {
                return
            }

            const trackFile = await fileResponse.blob()
            const trackFilename = `${trackFromApi.id} ${artist.username} ` +
                `- ${trackFromApi.title}.mp3`

            await supabase.storage.from('grabbed-tracks-files').upload(trackFilename,
                trackFile)
            const storageFileUrlResponse = supabase.storage.from('grabbed-tracks-files')
                .getPublicUrl(trackFilename)

            const newTrackMessage = `new track from ${artist.username} called ` 
                + `'${trackFromApi.title}' was saved. download here - ` 
                + storageFileUrlResponse.data.publicUrl

            await db.insert(tracks).values({
                id: trackFromApi.id,
                trackFileUrl,
                artistId: artist.id
            })

            newTracksReceivers.forEach(receiver => {
                if(trackFromApi.artwork_url) {
                    botApi.sendPhoto(receiver.id, 
                        getLargeArtworkUrl(trackFromApi.artwork_url),
                        {
                            caption: newTrackMessage
                        }
                    )
                }
                else {
                    botApi.sendMessage(receiver.id, newTrackMessage)
                }
            })
        }
    })
}

function getLargeArtworkUrl(artworkUrl : string) {
    /*
        soundcloud api returns that type of tracks 
        artwork (pic) - https://i1.sndcdn.com/artworks-435uih3u6ngfi-de64hj-large.jpg.
        despite the suffix '-large.jpg', the resolution is small asf.
        
        a larger size picture stored at urls with this
        format - https://i1.sndcdn.com/artworks-435uih3u6ngfi-de64hj-t500x500.jpg
    */
    const SMALL_ARTWORK_ENDING = '-large.jpg'
    return artworkUrl.slice(0, -SMALL_ARTWORK_ENDING.length) + '-t500x500.jpg'
}
