import { relations, InferModel } from 'drizzle-orm'
import { integer, pgTable, serial, text } from 'drizzle-orm/pg-core'
import { artists, Artist } from './artists.js'

export const tracks = pgTable('tracks', {
    id: integer('id').primaryKey().notNull(),
    trackFileUrl: text('track_file_url').notNull(),
    artistId: integer('artist_id'),
})

export const tracksRelations = relations(tracks, relationTypes => ({
    artist: relationTypes.one(artists, {
        fields: [ tracks.artistId ],
        references: [ artists.id ]
    })
}))

export type Track = InferModel<typeof tracks>
