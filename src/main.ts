import dotenv from 'dotenv'
import TelegramApi from 'node-telegram-bot-api'
import commands from './commands/commands.js'
import { ProcessedMessage } from './types/command.js'
import cron from 'node-cron'
import { grabNewTracks } from './cron-jobs/grab-new-tracks.js'
import { createServer } from 'http'
import { updateSoundcloudClientId } from './cron-jobs/update-soundcloud-client-id.js'

const server = createServer((request, response) => {
    response.end('123')
})

server.listen(3000)

dotenv.config()

const botApiToken = process.env['TELEGRAM_BOT_API_TOKEN']

if(!botApiToken) {
    throw new Error('Environment variable \'TELEGRAM_BOT_API_TOKEN\' was not provided')
}

const botApi = new TelegramApi(botApiToken, { polling: true })

botApi.on('message', message => {
    if(message.from?.username !== 'disgracing') {
        botApi.sendMessage(message.chat.id, 'u r not allowed mf')
        return
    }

    commands.forEach(command => {
        if(message.text && message.text.startsWith('/' + command.name)) {
            command.serve(botApi, message as ProcessedMessage)
        }
    })
})

cron.schedule('*/1 * * * *', async () => {
    try {
        await grabNewTracks(botApi)
    }
    catch(error) {
        console.log(error)
    }
})
cron.schedule('0 1 * * *', async () => await updateSoundcloudClientId())